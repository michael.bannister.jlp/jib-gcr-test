package org.bannisters

class App {
    val greeting: String?
        get() {
            return "Hello world."
        }
}

@Suppress("UnusedMainParameter")
fun main(args: Array<String>) {
    println(App().greeting)
}
